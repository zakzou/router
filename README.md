router
======

router是一个go语言的路由分发包


## 特性

* 支持自定义正则匹配路由
* 支持restful
* 支持Middleware
* 支持Hook

## 安装

    go get -u github.com/zakzou/router


## 快速开始

### 1. 实例化路由
```go
import (
    "github.com/zakzou/router"
)

r := router.NewRouter()
```

#### 1.1 默认不会严格匹配斜杠，如果需要匹配，则需要设置
```go
r.StrictSlash(true)
```

#### 1.2 也可以使用连缀方式
```go
r := route.NewRouter().StrictSlash(true)
```

### 2. 注册路由
```go
home := r.HandleFunc("/home", func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprint(w, "home")
})
```

#### 2.1 默认路只支持GET访问，如果需要支持POST，GET同时访问，可以通过Methods方法
```go
home.Methods("POST", "GET")
```

#### 2.2 也可以通过连缀方式，也可也支持单个路由不严格匹配斜杠
```go
r.HandleFunc("/home", func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprint(w, "home")
}).Methods("GET").StrictSlash(false)
```

#### 2.3 注册命名路由
```go
home.Name("home")
```

#### 2.4 路由也支持模式匹配

* 支持三种默认模式`int`, `any`, `string`
* 也可以自定义则正`<[pattern:]paramName>`

```go
r.HandleFunc("/<param1>/<string:params2>/<int:user_id>/", func(w http.ResponseWriter, r *http.Request) {
    query := r.URL.Query()
    param1 := query.Get("param1")
    param2 := query.Get("param2")
    param3 := query.Get("param3")
    fmt.Fprintln(w, param1, param2, param3)
})
```

### 3. 注册中间件(Middleware)

中间件会在路由被调用之前调用，一个路由可以注册多个中间件

```go
home.MiddlewareFunc(func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintln("Middleware 1")
})
```

#### 3.1 也可以为所有路由注册中间件

```go
r := router.NewRouter().MiddlewareFunc(func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintln("Middleware 0")
})
```

### 4. 注册钩子

钩子会在执行路由的前后调用，目前支持2对钩子

* `HookBeforeRouter` 每次请求前，无论路由是否存在，都会执行，在`HookBeforeDispatch`之前
* `HookAfterRouter` 每次请求后，无论路由是否存在，都会执行，在`HookAfterDispatch`之后
* `HookBeforeDispatch` 路由匹配成功后，路由分发之前执行
* `HookAfterDispatch` 路由匹配成功后，路由分发之后执行

```go
r.HookFunc(HookAfterDispatch, func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprint(w, "hook")
})
```

### 5. 生成地址

`UrlFor` 可以为命名路由自动生成地址

```go
r.HandleFunc("/user/profile/query/<int:user_id>/", func(w http.ResponseWriter, r *http.Request) {
}).Name("profile")

if urls, ok := r.UrlFor("profile", map[string]interface{}{"user_id": 100001}); ok {
    println(urls)
}
```

### 6. 运行

```go
if err := http.ListenAndServe(":9999", r); err != nil {
    println(err.Error())
}
```
